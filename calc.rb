#!/usr/bin/env ruby
# ip calculator
#
# Subnets (by mask group, all by 1 mask ):
# Net intersections:
# add range support

require 'optparse'
require 'ipaddress'
require_relative 'lib/printer'
require_relative 'lib/actions'
require_relative 'lib/support_methods'
require_relative 'lib/help_messages'
require_relative 'lib/option_handling'

options = {}

def read_files(fnames, debug)
  data = ''
  fnames.each do |value|
    if File.exist?(value)
      puts "Found file #{value}" if debug
      File.open(value, 'r') do |infile|
        while (line = infile.gets)
          puts "Added address (not from file): #{line}" if debug
          data += line
        end
      end
    else
      puts "Added address (not from file): #{value}" if debug
      data += value + ' '
    end
  end
  data
end

def get_ip(text, no_uniq, debug)
  ip_data = text.scan %r{(?<![\/\d\.])(?:\d{1,3}\.){3}\d{1,3}(?:\/\d+)?(?:(?:\.\d+){3})?}
  puts "Ip adresses found in source data: #{ip_data}" if debug
  ip_data = ip_data.map do |el|
    begin
      IPAddress::IPv4.new(el)
    rescue ArgumentError
      puts "#{el} is not correct ip address"
    end
  end.compact
  ip_data.uniq!(&:to_string) unless no_uniq
  puts "There were found ip: #{ip_data.map(&:to_string)}" if debug

  ip_data = nil if ip_data.empty?
  ip_data
end

def get_netmasks(text, no_uniq, debug)
  mask_data = text.scan(%r{(?<![\/\d])\/(?:\d{1,3}\.?){3}\d{1,3}})
  data = text.gsub(%r{(?<![\/\d])\/(?:\d{1,3}\.?){3}\d{1,3}}, '')
  mask_data += data.scan(%r{(?<![\/\d])\/\d{1,2}})
  puts "Netmasks found in source data: #{mask_data}" if debug
  mask_data.uniq! unless no_uniq
  mask_data
end

def get_addresses(text, options, no_uniq = false, debug = false, check_mask = false)
  data = ''
  fnames = text.split(/[\s,;]/)
  data += read_files(fnames, debug)
  ip_data = get_ip(text, no_uniq, debug)
  if ip_data.nil? && check_mask
    puts 'There were not found any ip adresses. Trying search netmasks' if debug
    mask_data = get_netmasks(data, no_uniq, debug)
    options[:operation] = :netmask_info unless mask_data.nil?
    puts "Resulted netmasks: #{mask_data}" if debug
  end
  ip_data || mask_data
  # values.each do |value|
  # if File.exist?(value)
  # puts "Found file #{value}" if debug
  # File.open(value, 'r') do |infile|
  # while (line = infile.gets)
  # puts "Added address (not from file): #{line}" if debug
  # data += line
  # end
  # end
  # else
  # puts "Added address (not from file): #{value}" if debug
  # data += value + ' '
  # end
  # end
  # searhing all ip addresses/nets in source text
  # if ip_data.nil? && check_mask
  # mask_data = data.scan(/(?<![\/\d])\/(?:\d{1,3}\.?){3}\d{1,3}/)
  # data = data.gsub(/(?<![\/\d])\/(?:\d{1,3}\.?){3}\d{1,3}/, '')
  # mask_data += data.scan(/(?<![\/\d])\/\d{1,2}/)
  # end
end

#===========================================
# set default options values and declare variables
options[:help] = !ARGV.any?
help_message = nil
options[:operation] ||= :info
# options[:kind] ||= ''

#===========================================
# parse options

OptionParser.new do |opts|
  opts.banner = HELP_BANNER
  opts.on('--debug', 'debug') do |_deb|
    options[:debug] = true
  end
  opts.on('-s', '--src-group DATA', OPTIONS_SOURCE_DESCRIPTION) do |s|
    options[:src] = s
  end
  opts.on('-t', '--target-group DATA',
          OPTIONS_TARGET_DESCRIPTION) \
           { |t| options[:trg] = t }
  opts.on('-o', '--operation OPT', [:info, :includes, :supernet, :summarization, :subnet, :split],
          OPTIONS_OPERATIONS_DESCRIPTION) \
           { |o| options[:operation] = o }
  opts.on('-n', '--number DATA', '--netmask DATA',
          OPTIONS_NUMBER_DESCRIPTION) \
           { |n| options[:number] = n } # [*n].map(&:to_i) }
  opts.on('-d', '--detailed',
          OPTIONS_DETAILED_DESCRIPTION) \
           { |_dt| options[:detailed] = true }
  opts.on('-k', '--kind KIND,[NOTATION]', Array,
          OPTIONS_KIND_DESCRIPTION) \
           { |k| options[:kind] = k }
  opts.on('-h', '--hide-title',
          OPTIONS_HIDE_DESCRIPTION) \
           { |_h| options[:hide] = true }
  opts.on('--help [ATTR],[ATTR2]', Array,
          OPTIONS_HELP_DESCRIPTION) \
           { |h| options[:help] = h || true }
  opts.on('--nocolor', \
          OPTIONS_NOCOLOR_DESCRIPTION) \
           { |_nc| options[:nocolor] = true }
  opts.on('--nouniq', \
          OPTIONS_NOUNIQ_DESCRIPTION) \
           { |_nu| options[:nouniq] = true }
  opts.on('--version', \
          OPTIONS_VERSION_DESCRIPTION) \
           { |_ver| options[:version] = true }

  help_message = opts # find good method to copy only text
end.parse!
#===========================================
options[:src] = ARGV[0] unless ARGV.empty? # parse arg input for is will be abble to call without -s argument
# fill data
options[:src] = get_addresses(options[:src], options, options[:nouniq], options[:debug], true) if options [:src]
options[:trg] = get_addresses(options[:trg], options, options[:nouniq], options[:debug]) if options[:trg]

puts "Program is runned with params: #{options}" if options[:debug]

if options[:help]
  print_help options[:help], help_message
elsif options[:version]
  puts VERSION_INFO
else
  errors = OptionChecker.new(options).valid?
  unless errors.empty?
    puts errors.join("\n")
    exit
  end
  results, errors = case options[:operation]
                    when :includes
                      net_includes(options[:src], options[:trg], options[:detailed], options[:debug])
                    when :info
                      net_info(options[:src], options[:detailed], options[:kind], options[:debug])
                    when :supernet
                      supernet(options[:src], options[:number], options[:detailed], options[:debug])
                    when :summarization
                      summarization(options[:src], options[:debug])
                    when :subnet
                      subnets(options[:src], options[:number], options[:debug])
                    when :split
                      split(options[:src], options[:number], options[:debug])
                    when :netmask_info
                      netmask_info(options[:src], options[:debug])
                    end
  printer = Printer.new
  printer.push_groups results
  printer.common_footter = errors
  printer.print_all(options[:nocolor], options[:hide])
end
