def netmask_info(src, debug = false)
  puts "There were recieved netmasks #{src}" if debug
  errors = []
  res = []
  src.each do |nm|
    begin
      addr = IPAddress::IPv4.new('192.168.1.0' + nm)
    rescue ArgumentError
      text = "Incorrect input '#{nm}'. -s parametre with '/' shoud have format '/x' or '/x.x.x.x' and be valid netmask"
      errors << text
      next
    end
    res << { header: nm, title: 'netmask:', body: get_netmask(addr, ['short', 10, 2, 8, 16]) }
    res << { title: 'host count:', body: get_host_count(addr) }
    res << { title: 'wildcard', body: get_wildcard(addr, [10, 2, 8, 16]) }
  end
  [res, errors]
end

def net_includes(src, trgt, detailed, _debug = false)
  including = []
  src.product(trgt) { |e1, e2| including.push([e1, e2]) if e2.include?(e1) }
  res = []
  errors = []
  all_included = including.map { |el| el[0] }
  res << { title: 'All included nets:', body: all_included.map(&:to_string) }

  res << { title: 'All nets, which not included in another:',
           body: (src - all_included).map(&:to_string) }

  if detailed
    res << { title: 'All nets, which not included in another:',
             body: including.map do |netpair|
                     format('%-18s >>included in>> %-18s',
                            netpair[0].to_string.to_s, netpair[1].to_string.to_s)
                   end }

    res << { title: 'Networks, which not include any ones:',
             body: (trgt - including.map { |el| el[1] }).map(&:to_string) }
  end
  [res, errors]
end

def data_by_ktype(ktype, knotate, ip, debug = false)
  puts "kind is #{ktype}, and notation is #{knotate}" if debug
  knotate ||= '10'
  knotate = knotate.to_i if knotate != 'short'

  case ktype
  when 'address', 'a', 'addr'
    { header: ip.to_string, body: get_addr(ip, knotate) }
  when 'netmask', 'nm', 'mask', 'n'
    { header: ip.to_string, body: get_netmask(ip, knotate) }
  when 'broadcast', 'br'
    { header: ip.to_string, body: get_broadcast(ip, knotate) }
  when 'min_host', 'min'
    { header: ip.to_string, body: get_min_host(ip, knotate) }
  when 'max_host', 'max'
    { header: ip.to_string, body: get_max_host(ip, knotate) }
  when 'host_count', 'hc'
    { header: ip.to_string, body: get_host_count(ip, knotate) }
  when 'wildcard', 'wc'
    { header: ip.to_string, body: get_wildcard(ip, knotate) }
  when 'reverseaddress', 'ra'
    { header: ip.to_string, body: get_reverse_addr(ip, knotate) }
  end
end

# just 2 sup methods
def get_detailed_info_list(ip)
  res = []
  res << { title: 'address:',              body: get_addr(ip, [10, 2, 16, 8]) }
  res << { title: 'netmask:',              body: get_netmask(ip, ['short', 10, 2, 16, 8]) }
  res << { title: 'wildcard:',             body: get_wildcard(ip, [10, 2, 16, 8]) }
  res << { title: 'network address:',      body: get_net_addr(ip, [10, 2, 16, 8]) }
  res << { title: 'broadcast address:',    body: get_broadcast(ip, [10, 2, 16, 8]) }
  res << { title: 'host min:',             body: get_min_host(ip, [10, 2, 16, 8]) }
  res << { title: 'host max:',             body: get_max_host(ip, [10, 2, 16, 8]) }
  res << { title: 'maximum hosts count:',  body: get_host_count(ip, [10, 2, 16, 8]) }
  res << { title: 'reverse zone address:', body: get_reverse_addr(ip, [10, 2, 16, 8]) }
  res
end

def get_info_list(ip)
  res = []
  res << { title: 'address:',              body: get_addr(ip) }
  res << { title: 'netmask:',              body: get_netmask(ip, ['short', 10]) }
  res << { title: 'network address:',      body: get_net_addr(ip) }
  res << { title: 'host min:',             body: get_min_host(ip) }
  res << { title: 'host max:',             body: get_max_host(ip) }
  res << { title: 'maximum hosts count:',  body: get_host_count(ip) }
  res
end

def net_info(nets, detailed, kind = nil, debug = false)
  puts "Recieved nets in #net_info: #{nets.inspect}, recieved kind: #{kind.inspect}" if debug
  res = []
  errors = []
  nets.each do |ip|
    if !kind.nil? # when we want to concrete information
      ktype, knotate = kind
      res << data_by_ktype(ktype, knotate, ip, debug)
    else
      res << { header: ip.to_string }
      res += if detailed
               get_detailed_info_list(ip)
             else
               get_info_list(ip)
             end
    end
  end
  [res, errors]
end

def supernet(src_nets, netmasks, detailed, _debug = false)
  res = []
  errors = []
  err_count = 0
  res << { header: 'All supernets:' }
  src_nets.each do |ip|
    begin
      res << if detailed
               { body: (format('%-18s', ip.to_string) +
                        ' => ' +
                        format('%-18s', ip.supernet(netmasks.to_i).to_string)) }
             else
               { body: ip.supernet(netmasks.to_i).to_string }
             end
    rescue StandardError => e
      raise e unless e.message == 'New prefix must be smaller than existing prefix'
      err_count += 1
    end
  end
  text = "There were #{err_count} nets, which not handled because of target subnet mask greater then source"
  errors << text if err_count > 0
  [res, errors]
end

def summarization(src_nets, _debug = false)
  res = []
  errors = []
  res << { body: IPAddress::IPv4.summarize(*src_nets).map(&:to_string) }
  [res, errors]
end

def subnets(src, netmask, debug = false)
  puts "Mehod subnets called with src: #{src.inspect}, netmask: #{netmask.inspect}" if debug
  res = []
  errors = []
  err_count = 0
  src.each do |ip|
    begin
      res << { title: ip.to_string, body: ip.subnet(netmask.to_i).map(&:to_string) }
    rescue StandardError => e
      raise e unless e.message.include?('New prefix must be between ')
      err_count += 1
    end
  end
  text = "There were #{err_count} nets, which not handled because target subnet mask smaller then source"
  errors << text if err_count > 0
  [res, errors]
end

def split(src, count, debug = false)
  puts "Split running with src: #{src.inspect} \n count: #{count.inspect}" if debug
  res = []
  errors = []
  src.each do |ip|
    begin
      res << { title: ip.to_string, body: ip.split(count.to_i).map(&:to_string) }
    rescue ArgumentError => e
      raise e unless  e.message.include?('out of range')
      errors << "It not possible to split net #{ip.to_string} on #{count} networks"
    end
  end
  [res, errors]
end
