def colorize(color_code, text)
  "\e[#{color_code}m#{text}\e[0m"
end

def title(text)
  "\n" + colorize(GREEN, text.upcase) + "\n"
end

def supertitle(text)
  "\n" + colorize(BG_BLUE, text.upcase)
end

def br
  "\n"
end

HELP_BANNER = 'Advanced ip calculator. This ip calculator implement extended functions of '\
              "working with ip-addressing.\n"\
              'Created by HAFF'.freeze
#======================================
# main help options
OPTIONS_SOURCE_DESCRIPTION   = 'Set source group or file for operation. "--help src-group" or "--help s" for details.'
                               .freeze
OPTIONS_TARGET_DESCRIPTION   = 'Set target group or file for operation. "--help target-group" or '\
                               '"--help t" for details.'.freeze
OPTIONS_OPERATIONS_DESCRIPTION = "Choose operation type. Option is required. Avaible types:
        #{format('%-31s - %s', colorize(GREEN, 'info'), 'Show common information.')}
        #{format('%-31s - %s', colorize(GREEN, 'includes'), 'Show info about sources nets includes in targets ones.')}
          If scan of one list required - send it as source and target parametre both.
        #{format('%-31s - %s', colorize(GREEN, 'supernet'), 'Get supernet of each subnets.')}
        #{format('%-31s - %s', colorize(GREEN, 'summarization'), 'Produce aggregation of networks.')}
        #{format('%-31s - %s', colorize(GREEN, 'subnet'), 'Split netswork to subnets with passed netmask.')}
        #{format('%-31s - %s', colorize(GREEN, 'split'), 'Split netswork to selected subnets count.')}".freeze
OPTIONS_NUMBER_DESCRIPTION   = 'Netmask group or additional numeric peremeter for some types of opertaion'.freeze
OPTIONS_DETAILED_DESCRIPTION = 'Show detailed output. "--help d" or "--help details" for more info'.freeze
OPTIONS_KIND_DESCRIPTION     = 'For info operation only. Select one or more output data type. "--help k" or '\
                               '"--help kind" for details'.freeze
OPTIONS_HIDE_DESCRIPTION     = 'Hide titles, comments and another note fields. If you are going to '\
                               'use app for automatisation - probably this option will be useful'.freeze
OPTIONS_HELP_DESCRIPTION     = "Show this help. Try #{colorize(MARGENTA, '--help [PARAMETRE1],"\
                               "[SUBPARAMETRE]')} (without space) to show help about command".freeze
OPTIONS_NOCOLOR_DESCRIPTION  = 'Disable color output.'.freeze
OPTIONS_VERSION_DESCRIPTION  = 'Show application version.'.freeze
OPTIONS_NOUNIQ_DESCRIPTION   = 'By default calculator remove duplicates in received data. '\
                               'You can to pass this key for disable doing that.'.freeze

#======================================
# specified commands help
OPTION_SRC_GROUP_HELP = "Usage:
#{colorize(YELLOW, '--src-group <DATA>')}
#{colorize(YELLOW, '--s <DATA>')}

Set source group or file for operation. Each address/network can be delimited any symbols, exlude"\
" numbers, '/' and '.' (source data will be parsed by regexp). Avaivle formats of nets/hosts:
  -10.200.10.26
  -10.200.10.0/24
  -10.200.10.0/255.255.255.0
Examples:
inline call:
  #{colorize(CYAN, 'ipcalc -s \'192.168.50.1/24 | 192.168.50.2, 192.168.51.0/255.255.255.0 \'')}
use prepared file:
  #{colorize(CYAN, 'ipcalc -s src_file_name.in')}
'-s' key can be omitted:
  #{colorize(CYAN, 'ipcalc \'192.168.50.1/24 | 192.168.50.2, 192.168.51.0/255.255.255.0 \'')}

It is abble to get iformation about netmask. You should pass parametre -s started from '/' symbol in order to do it.
  #{colorize(CYAN, 'ipcalc -s /24')}
  #{colorize(CYAN, 'ipcalc /255.255.255.0')}
  #{colorize(CYAN, 'ipcalc \'/22 /255.255.0.0\'')}".freeze

OPTION_TARGET_GROUP_HELP = "Usage:
#{colorize(YELLOW, '--target-group <DATA>')}
#{colorize(YELLOW, '-t <DATA>')}

Set target group or file for operation. Example:
  #{colorize(CYAN, 'ipcalc -o includes -s \'10.200.1.25 10.155.10.150 10.155.10.193/27\' '\
  '-t \'10.155.2.0/24 10.155.10.128/25\'')}
  #{colorize(CYAN, 'ipcalc -o includes -s \'10.200.1.25 10.155.10.150 10.155.10.193/27\' -t file.in')}
will print info about includes of nets from source group into target".freeze

OPTION_NUMBER_HELP = "Usage:
#{colorize(YELLOW, '--number <NUM>')}
#{colorize(YELLOW, '--netmask <NETMASK>')}
#{colorize(YELLOW, '-n <NUM>')}

This parametre use for set numeric values for some operations (netmask, count etc.).
Can be used with operations: split, subnets, supernets. See help by operation for more information".freeze

OPTION_DETAILED_HELP = "Usage:
#{colorize(YELLOW, '--detailed')}
#{colorize(YELLOW, '-d')}

Parametre for print detailed result.
Can be used with: includes, supernet calc operations.
See help by operation for more information.
Does not need to pass value".freeze

OPTION_HIDE_TITLE_HELP = "Usage:
#{colorize(YELLOW, '--hide-title')}
#{colorize(YELLOW, '-h')}

Minimize output. Remove some titles and comments. Usefull for automatisation. Does not need to pass value.
Can be used with operations: includes, subnets, supernets, split".freeze

OPTION_KIND_HELP = "Usage:
#{colorize(YELLOW, '--kind DATA_TYPE,[NOTATION] ')}
#{colorize(YELLOW, '--k DATA_TYPE,[NOTATION] ')}

Specified output data format. Designed in the main for automatic using and scripting."\
" If were recieved multiple addresses - info will print spase-separated
Avaible types:
  #{colorize(YELLOW, 'address, a, addr')} - get ip address
  #{colorize(YELLOW, 'netmask, nm, mask')} - get netmask
  #{colorize(YELLOW, 'broadcast, br')} - get broadcast address
  #{colorize(YELLOW, 'min_host, min')} - minimum host address
  #{colorize(YELLOW, 'max_host, max')} - maximum host address
  #{colorize(YELLOW, 'host_count, hc')} - get maximum host count in network
  #{colorize(YELLOW, 'wildcard, wc')} - network wildcard
  #{colorize(YELLOW, 'reverseaddress, ra')} - reverse zone dns address
Avaible formats(required not for all types):
  #{colorize(YELLOW, '2')} - binary
  #{colorize(YELLOW, '10')} - decimal
  #{colorize(YELLOW, '8')} - octal
  #{colorize(YELLOW, '16')} - hexadecimal
  #{colorize(YELLOW, 'short')} - only for netmask. short mask format (ex. /24 /32)
Examples:
  #{colorize(CYAN, 'ipcalc -s \'10.100.50.1\' -o info -k \'address 2\'')}    -"\
    " print address in binary format for each net/host from input_file.txt
  #{colorize(CYAN, 'ipcalc -s \'input_file.txt\' -o info -k \'a 10\'')}         - the same in decimal
  #{colorize(CYAN, 'ipcalc -s \'10.100.50.1\' -o info -k \'addr 16\'')}      - in hexadecimal
  #{colorize(CYAN, 'ipcalc input_file.txt -k addr')}                            - short call
  #{colorize(CYAN, 'ipcalc -s \'10.100.50.1\' -o info -k \'nm 10\'')}        - netmask in decimal
  #{colorize(CYAN, 'ipcalc -s \'10.100.50.1\' -o info -k \'mask short\'')}   - netmask in short format
  #{colorize(CYAN, 'ipcalc -s \'input_file.txt\' -o info -k host_count')}       - print hosts count.
  #{colorize(CYAN, 'ipcalc -s \'10.100.50.1\' -o info -k \'host_count 2\'')} - "\
    "print hosts count. 2nd parametr will be ignored
Can be used with operations: info".freeze

OPTION_OPERATION_HELP = "Usage:
#{colorize(YELLOW, '--operation')}
#{colorize(YELLOW, '-o')}

#{OPTIONS_OPERATIONS_DESCRIPTION}
#{colorize(MARGENTA, 'Try --help o,[TYPE], --help operation,[TYPE] '\
'or --help [TYPE] for watch more information and examples by specified operation')}".freeze

OPTION_NOCOLOR_HELP = "Usage:
#{colorize(YELLOW, '--nocolor')}
Disable colorize output".freeze
OPTION_NOUNIQ_HELP = "Usage:
#{colorize(YELLOW, '--nouniq')}
By default calculator remove duplicates of data from passed lists. This options disable to do it".freeze
OPTION_VERSION_HELP = "Usage:
#{colorize(YELLOW, '--version')}
Print current version of application".freeze

#======================================
# operation types
OPTION_INFO_HELP = "#{colorize(YELLOW, 'INFO')}
#{colorize(YELLOW, '-o info --source DATA [--detailed] [--kind TYPE,NOTATION] [--nouniq] [--nocolor] [--debug]')}
Traditional operation. Print common data in different notation (binary, octal etc.) about source network. Such as:
  -address
  -netmask
  -wildcard
  -network address
  -min/max host
  -broadcast
  -host count
  -reverse zone addr
Example:
  #{colorize(CYAN, 'ipcalc -s \'192.168.50.1/24 192.168.50.2, 192.168.51.0/255.255.255.0\' -o info -d')}
There will show more information if key '-d' is present.
Also options kind can be used with this operation. Read #{colorize(MARGENTA, '--help k ')}
It is default options and can be omited.
  #{colorize(CYAN, 'ipcalc -s file.in -d')} or
  #{colorize(CYAN, 'ipcalc file.in -d')}

It is abble to get information by mask.
  #{colorize(CYAN, 'ipcalc /19')}
  #{colorize(CYAN, 'ipcalc /255.255.255.0')}
will print information about netmask in different notation, wildcard and count of hosts".freeze

OPTION_INCLUDES_HELP = "#{colorize(YELLOW, 'INCLUDES')}
#{colorize(YELLOW, '-o includes --source DATA --target DATA [--detailed] [--hide] [--nouniq] [--nocolor] [--debug]')}
This method check which network from source group included in networks of target group.
The operation show not included nets and nets which not include any ones too.

  #{colorize(CYAN, 'ipcalc -s checked_nets -t all_nets.trgt -o includes')}
  #{colorize(CYAN, 'ipcalc -o includes -s \'10.200.1.25 10.155.10.150 10.155.10.193/27\' '\
  '-t \'10.155.2.0/24 10.155.10.128/25\'')}
operation with key '-d' will show each including and additional information.".freeze

OPTION_SUPERNET_HELP = "#{colorize(YELLOW, 'SUPERNET')}
#{colorize(YELLOW, '-o supernet --source DATA --netmask NETMASK [--detailed] [--nouniq] [--nocolor] [--debug]')}
This method search supernet with recieved netmask for each net from group.
IF NETMASKS LENGTH OF SOURCE NET GREATER THEN RECIEVED - THIS NET WILL BE IGNORED.
  #{colorize(CYAN, 'ipcalc -s \'10.201.100.0/24 10.201.100.1/27\' -o super -d -n 23')}
  #{colorize(CYAN, 'ipcalc -s file.in -o super -d -n 23')}".freeze

OPTION_SUBNETS_HELP = "#{colorize(YELLOW, 'SUBNET')} operation
#{colorize(YELLOW, '-o subnet -source DATA --netmask NETMASK [--nouniq] [--nocolor] [--debug]')}
The subnet method divide network (or each of networks) into subnets with given netmask.
For example network 10.161.15.40/24 will be splitted into 2 subnets by 25 mask: 10.161.15.0/25 and 10.161.15.128/25.

#{colorize(CYAN, 'ipcalc -s \'10.1.4.0/24 10.1.2.1/255.255.255.0\' -n 26 -o sub
=>10.1.4.0/24
  10.1.4.0/26
  10.1.4.64/26
  10.1.4.128/26
  10.1.4.192/26

  10.1.2.1/24
  10.1.2.0/26
  10.1.2.64/26
  10.1.2.128/26
  10.1.2.192/26')}".freeze

OPTION_SUMMARIZATION_HELP = "#{colorize(YELLOW, 'SUMMARIZATION')}
#{colorize(YELLOW, '-o summarization -source DATA [--nouniq] [--nocolor] [--debug]')}
Implement standart networks summarization. Summarize networks into nets with less netmask
#{colorize(CYAN, 'ipcalc -s \'10.1.0.0/24 10.1.1.1/24,10.1.2.1/255.255.255.0 10.1.3.1/24\' -o summ')}
=>10.1.0.0/22
#{colorize(CYAN, 'ipcalc -s \'10.1.0.0/24 10.1.1.1/24,10.1.2.1/255.255.255.0\' -o summ')}
=>10.1.0.0/23
  10.1.2.0/24".freeze

OPTION_SPLIT_HELP = "#{colorize(YELLOW, 'SPLIT')}
#{colorize(YELLOW, '-o split -source DATA [--nouniq] [--nocolor] [--debug]')}
Split selected networks on choosen count of subnets.
#{colorize(CYAN, 'ipcalc -s 10.212.1.2/24,10.211.1.1/20 -o split -n 5')}
=>10.212.1.2/24
  10.212.1.0/27
  10.212.1.32/27
  10.212.1.64/27
  10.212.1.96/27
  10.212.1.128/25
=>10.211.1.1/20
  10.211.0.0/23
  10.211.2.0/23
  10.211.4.0/23
  10.211.6.0/23
  10.211.8.0/21".freeze

VERSION_INFO = 'Advanced ip calculator by Haff
0.8 version from 15.03.2016'.freeze
