def get_help_name(attr)
  ('OPTION_' + attr.to_s.upcase.tr('-', '_') + '_HELP')
end

class OptionNamer
  OLIST = {
    'src-group':    [:src, :s, :'src-group'],
    'target-group': [:t, :'target-group'],
    operation:      [:o, :operation],
    number:         [:n, :number, :netmask],
    detailed:       [:d, :detailed],
    kind:           [:k, :kind],
    'hide-title':   [:h, :'hide-title'],
    nocolor:        [:nocolor],
    nouniq:         [:nouniq],
    version:        [:version],
    includes:       [:includes],
    info:           [:info],
    split:          [:split],
    subnets:        [:sub, :subnet, :subnets],
    supernet:       [:sup, :supernet, :super],
    summarization:  [:summarization, :summ, :sum]
  }.freeze

  def self.get_name(name)
    OLIST.select { |_k, v| v.map { |el| [el, el.to_s] }.flatten.include?(name) }.keys[0] || name
  end

  def self.get_variants(*name_list)
    name_list.map { |el| OLIST[el.to_sym] }.flatten
  end
end

class OptionChecker
  def initialize(options)
    @opt = options
    @errors = []
  end

  def valid?
    validate_number_present
    validate_number_is_integer
    validate_number_is_netmask
    validate_k_with
    validate_ktype_in_list
    validate_knotate_in_list
    validate_src_is_present
    validate_trg_not_nil_if_present
    validate_trg_present
    @errors
  end

  private

  def validate_number_present
    opts = *OptionNamer.get_variants(:supernet, :subnets, :split)
    return true unless opts.include? @opt[:operation]
    return if @opt.key?(:number)
    @errors << "For #{@opt[:operation]} operation you should pass '--number (-n)' param"
  end

  def validate_number_is_integer
    opts = *OptionNamer.get_variants(:supernet, :subnets, :split)
    return true unless opts.include? @opt[:operation]
    return unless @opt.key?(:number)
    if begin
         false if Integer(@opt[:number])
       rescue
         true
       end # only when throw exception
      @errors << '--number (-n) param shold be integer in this usage'
    end
  end

  def validate_number_is_netmask
    opts = *OptionNamer.get_variants(:supernet, :subnets)
    return true unless opts.include? @opt[:operation]
    return unless @opt.key?(:number)
    unless (1..32).cover?(@opt[:number].to_i)
      error_mess = "--number (-n) param for #{@opt[:operation]} is netmask and should be between 1 and 32"
      @errors << error_mess
    end
  end

  def validate_k_with
    opts = *OptionNamer.get_variants(:info)
    return true unless @opt.key?(:kind)
    return true if opts.include? @opt[:operation]
    @errors << "Option --kind (-k) can be used only with 'info' operation"
  end

  def validate_ktype_in_list
    return true unless @opt.key?(:kind)
    ktype, = @opt[:kind]
    types = %w(address a addr
               netmask nm mask
               broadcast br
               min_host min
               max_host max
               host_count hc
               wildcard wc
               reverseaddress ra)
    return if types.include? ktype
    @errors << "Type in --kind (-k) option incorrect. See '--help k' for more info"
  end

  def validate_knotate_in_list
    return true unless @opt.key?(:kind)
    _, knotate = @opt[:kind]
    notations = %w(2 8 10 16 short)
    unless knotate.nil? || notations.include?(knotate)
      @errors << "Notation in --kind (-k) option incorrect. See '--help k' for more info"
    end
  end

  def validate_src_is_present
    return true unless @opt[:src].nil? || @opt[:src].empty?
    @errors << "Parametre '-s' should have correct format and can not be empty. '--help s' for details"
  end

  def validate_trg_not_nil_if_present
    return true unless @opt.key?(:trg)
    return true unless @opt[:trg].nil? || @opt[:trg].empty?
    @errors << "Parametre '-t' should have correct format and can not be empty. '--help t' for details"
  end

  def validate_trg_present
    opts = *OptionNamer.get_variants(:includes)
    return true unless opts.include? @opt[:operation]
    return if @opt.key?(:trg)
    @errors << "For #{@opt[:operation]} method you should pass '--target-group (-t) parametre'"
  end
  # src not nil if present
  # trg not nil if present
  # trg presented for includes
  # # k only with info
  # # k type in correct list
  # # k notatr in list
  # # number for split present
  # # for subnet supernet number is integer
  # # for subnet supernet number is present
  # # number for split is integer
  # # for subnet supernet number isn 0..32

  # def check_params
  # errors = []
  # if @opt[:src].nil? || @opt[:src].empty?
  # errors << "Parametre '-s' should have correct format and can not be empty. '--help s' for details"
  # ktype, knotate = @opt[:kind]
  # end
  # if @opt.key?(:trg)
  # errors << "Parametre '-t' should have correct format and can not be empty. '--help t' for details"
  # end
  # if @opt.key?(:kind)
  # errors << "Option --kind (-k) can be used only with 'info' operation" if @opt[:operation] != :info
  # ktype, knotate = @opt[:kind]
  # types = %w(address a addr netmask nm mask broadcast br min_host min max_host max host_count hc wildcard
  #            wc reverseaddress ra)
  # notations = %w(2 8 10 16 short)
  # if !(types.include? ktype) || (!knotate.nil? && !notations.include?(knotate))
  # errors << "Format --kind (-k) option incorrect. See '--help k' for more info"
  # end
  # end
  # case @opt[:operation]
  # when :split
  # if @opt.key?(:number)
  # if begin
  # false if Integer(@opt[:number])
  # rescue
  # true
  # end only when throw exception
  # errors << '--number (-n) param shold be integer in this usage'
  # end
  # else
  # errors << "For #{@opt[:operation]} operation you should pass '--number (-n)' param"
  # end
  # when :includes -t present
  # errors << "For #{@opt[:operation]} method you should pass '--target-group (-t) parametre'" unless @opt.key?(:trg)
  # when *OptionNamer.get_variants(:supernet, :subnets)
  # if @opt.key?(:number)
  # if begin
  # false if Integer(@opt[:number])
  # rescue
  # true
  # end only when throw exception
  # errors << '--number (-n) param shold be integer in this usage'
  # else
  # unless (1..32).cover?(@opt[:number].to_i)
  # error_mess = "--number (-n) param for #{@opt[:operation]} is netmask and should be between 1 and 32"
  # errors << error_mess
  # end
  # end
  # else
  # errors << "For #{@opt[:operation]} operation you should pass '--number (-n)' param"
  # end
  # end
  # errors
  # end
end
