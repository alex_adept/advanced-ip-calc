BLACK = 30
RED = 31
GREEN = 32
YELLOW = 33
BLUE = 34
MARGENTA = 35
CYAN = 36
GRAY = 37
BG_BLACK = 41
BG_RED = 41
BG_GREEN = 42
BG_YELLOW = 43
BG_BLUE = 44
BG_MARGENTA = 45
BG_CYAN = 46
BG_GRAY = 47

class Printer
  attr_accessor :common_header
  attr_reader :main_data
  # [ {header: 'header here', title: 'tite here', body: ['line1', 'line2'], footter: ['footter_line1']},
  #   {title: 'tite here', body: ['line1', 'line2'], footter: ['footter_line1']} ]
  attr_reader :common_footter

  def initialize
    @common_header ||= ''
    @main_data ||= []
    @common_footter ||= []
  end

  def new_group
    @main_data << { body: [] }
  end

  def push_header(val)
    @main_data.last[:header] = val
  end

  def common_footter=(val)
    @common_footter = [*val]
  end

  def push_title(val)
    @main_data.last[:title] = val
  end

  def push_body(body_line)
    @main_data.last[:body] << body_line
  end

  def push_footter(val)
    @main_data.last[:footter] = val
  end

  def push_group(val)
    @main_data << val
  end

  def push_groups(val)
    @main_data += val
  end

  def print_all(nocolorize = false, hide_title = false)
    correct_user_input
    prepare_data nocolorize

    # START PRINTING
    puts @common_header if !@common_header.nil? && !@common_header.empty? && !hide_title
    @main_data.each do |el|
      if hide_title
        print_body(el, true)
        puts
      else
        print_header el
        print_title el
        print_body el
        print_footter el
      end
    end
    puts @common_footter
  end

  private

  def print_header(el)
    puts el[:header] if el[:header]
  end

  def print_title(el)
    puts el[:title] if el[:title]
  end

  def print_body(el, hide_title = false)
    return unless el[:body]
    if hide_title
      puts el[:body].map { |bd| bd }.join(' ')
    else
      puts el[:body].map { |bd| "\t" + bd }.join("\n")
    end
  end

  def print_footter(el)
    el[:footter].each { |ft| puts ft } if el[:footter]
  end

  def correct_user_input
    @main_data.each do |el|
      el[:footter] = [*el[:footter]] if el[:footter] # footter should to be []
      el[:body]    = [*el[:body]]    if el[:body] # body should to be []
    end
  end

  def prepare_data(nocolorize)
    unless nocolorize
      supertitle!(@common_header) if !@common_header.nil? && !@common_header.empty?
      @common_footter.map! { |foot| footter!(foot) } # []
      @main_data.each do |hash|
        hash.each do |k, val|
          case k
          when :header # ''
            supertitle!(val)
          when :title # ''
            title!(val)
          when :footter # []
            val.each { |foot| footter!(foot) }
          end
        end
      end
    end
  end

  def title(text)
    colorize(GREEN, text.upcase)
  end

  def title!(text)
    text.replace(colorize(GREEN, text.upcase))
  end

  def supertitle(text)
    "\n" + colorize(BG_BLUE, text.upcase)
  end

  def supertitle!(text)
    text.replace("\n" + colorize(BG_BLUE, text.upcase))
  end

  def footter(text)
    colorize(MARGENTA, text)
  end

  def footter!(text)
    text.replace(colorize(MARGENTA, text))
  end

  def colorize(color_code, text)
    "\e[#{color_code}m#{text}\e[0m"
    # 30 black
    # 31 red
    # 32 green
    # 33 yellow
    # 34 blue
    # 35 magenta
    # 36 cyan
    # 37 gray
    # 40 bg_black
    # 41 bg_red
    # 42 bg_green
    # 43 bg_yellow
    # 44 bg_blue
    # 45 bg_magenta
    # 46 bg_cyan
    # 47 bg_gray
    # def bold;           "\e[1m#{self}\e[22m" end
    # def italic;         "\e[3m#{self}\e[23m" end
    # def underline;      "\e[4m#{self}\e[24m" end
    # def blink;          "\e[5m#{self}\e[25m" end
    # def reverse_color;  "\e[7m#{self}\e[27m" end
  end

  private :colorize, :title, :supertitle, :title!, :supertitle!, :footter, :footter!, :correct_user_input, :prepare_data
end
