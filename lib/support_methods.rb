def print_operation_help(type)
  if type
    operation = OptionNamer.get_name(type)
    puts instance_eval(get_help_name(operation)) # will print OPTION_ + name + _help const
  else
    puts OPTION_OPERATION_HELP
  end
end

def print_help(h_opt, help_message)
  opt, operation = h_opt
  opt = OptionNamer.get_name(opt)
  case opt
  when true
    puts help_message
  when :operation
    print_operation_help operation
  else
    puts instance_eval(get_help_name(opt)) # will print OPTION_ + name + _help const
  end
end

def get_addr(net, notation = 10)
  octets = net.octets
  notation = [*notation]
  notation.map { |el| octets_to_notation(octets, el) }.flatten
  # octets_to_notation(octets,notation)
end

def get_netmask(net, notation = 'short')
  # res = ''
  notation = [*notation]
  notation.map do |nt|
    if nt == 'short'
      # res = net.prefix.to_s
      net.prefix.to_s
    else
      octets = ('1' * net.prefix.to_i).ljust(32, '0').scan(/.{1,8}/).map { |el| el.ljust(8, '0').to_i(2) }
      # res = octets_to_notation(octets, notation)
      octets_to_notation(octets, nt)
    end
  end.flatten
  # res
end

def get_net_addr(net, notation = 10)
  octets = IPAddress::IPv4.new(net.network.address).octets
  # octets_to_notation(octets, notation)
  notation = [*notation]
  notation.map { |el| octets_to_notation(octets, el) }.flatten
end

def get_broadcast(net, notation = 10)
  octets = net.broadcast.octets
  # octets_to_notation(octets, notation)
  notation = [*notation]
  notation.map { |el| octets_to_notation(octets, el) }.flatten
end

def get_min_host(net, notation = 10)
  octets = net.first.octets
  notation = [*notation]
  # octets_to_notation(octets, notation)
  notation.map { |el| octets_to_notation(octets, el) }.flatten
end

def get_max_host(net, notation = 10)
  octets = net.last.octets
  # octets_to_notation(octets, notation)
  notation = [*notation]
  notation.map { |el| octets_to_notation(octets, el) }.flatten
end

def get_host_count(net, notation = 10)
  notation = [*notation]
  notation.map { |nt| net.size.to_s(nt) }
  # unless no_newline
  # res.insert( 0, "\t" )
  # res += br
  # end
end

def get_wildcard(net, notation = 10)
  octets = net.prefix.octets.map { |el| 255 - el }
  # octets_to_notation(octets, notation)
  notation = [*notation]
  notation.map { |el| octets_to_notation(octets, el) }.flatten
end

def get_reverse_addr(net, notation = 10)
  # res = net.reverse
  # unless no_newline
  #  res.insert( 0, "\t" )
  #  res += br
  # end
  notation = [*notation]
  notation.map do |nt|
    res = octets_to_notation(net.octets.reverse, nt)
    res.to_s + '.in-addr.arpa'
  end
end

def octets_to_notation(octets_array, notation)
  notation ||= 10
  res = case notation
        when 2
          octets_array.map { |el| el.to_s(2).rjust(8, '0') }.join('.')
        when 8
          octets_array.map { |el| el.to_s(8) }.join('.')
        when 10
          octets_array.map { |el| el.to_s(10) }.join('.')
        when 16
          octets_array.map { |el| el.to_s(16) }.join('.')
        end
  # unless no_newline
  # res += "\n"
  # res.insert( 0, "\t" )
  # end
  res
end
