require 'ipaddress'

require_relative '../lib/actions'
require_relative '../lib/support_methods'

describe 'test_operation_methods' do
  def ip_address_group(addr)
    addr.split(/[,\s]/).map { |el| IPAddress::IPv4.new(el) }
  end

  def generate_ip
    res = ''
    3.times { res += Random.rand(255).to_s + '.' }
    res += Random.rand(255).to_s
  end

  let(:random_ip_address) do
    IPAddress::IPv4.new(generate_ip)
  end

  describe '#netmask_info' do
    it 'return valid result with valid prams' do
      res = netmask_info(['/255.255.255.254', '/18', '/255.255.255.0'])
      expect(res).to eq([
                          [{ header: '/255.255.255.254',
                             title: 'netmask:',
                             body: ['31',
                                    '255.255.255.254',
                                    '11111111.11111111.11111111.11111110',
                                    '377.377.377.376',
                                    'ff.ff.ff.fe'] },
                           { title: 'host count:',
                             body: ['2'] },
                           { title: 'wildcard',
                             body: ['0.0.0.1',
                                    '00000000.00000000.00000000.00000001',
                                    '0.0.0.1',
                                    '0.0.0.1'] },

                           { header: '/18',
                             title: 'netmask:',
                             body: ['18',
                                    '255.255.192.0',
                                    '11111111.11111111.11000000.00000000',
                                    '377.377.300.0',
                                    'ff.ff.c0.0'] },
                           { title: 'host count:',
                             body: ['16384'] },
                           { title: 'wildcard',
                             body: ['0.0.63.255',
                                    '00000000.00000000.00111111.11111111',
                                    '0.0.77.377',
                                    '0.0.3f.ff'] },

                           { header: '/255.255.255.0',
                             title: 'netmask:',
                             body: ['24',
                                    '255.255.255.0',
                                    '11111111.11111111.11111111.00000000',
                                    '377.377.377.0',
                                    'ff.ff.ff.0'] },
                           { title: 'host count:',
                             body: ['256'] },
                           { title: 'wildcard',
                             body: ['0.0.0.255',
                                    '00000000.00000000.00000000.11111111',
                                    '0.0.0.377',
                                    '0.0.0.ff'] }],
                          []
                        ])
    end
    it 'produce errors with invalid params' do
      res = netmask_info(['/255.255.255.253', '/33', '/255.255.255.0'])
      expect(res).to eq([
                          [{ header: '/255.255.255.0',
                             title: 'netmask:',
                             body: ['24',
                                    '255.255.255.0',
                                    '11111111.11111111.11111111.00000000',
                                    '377.377.377.0',
                                    'ff.ff.ff.0'] },
                           { title: 'host count:',
                             body: ['256'] },
                           { title: 'wildcard',
                             body: ['0.0.0.255',
                                    '00000000.00000000.00000000.11111111',
                                    '0.0.0.377',
                                    '0.0.0.ff'] }],
                          ["Incorrect input '/255.255.255.253'. -s parametre with '/' shoud have format '/x' or \
'/x.x.x.x' and be valid netmask", "Incorrect input '/33'. -s parametre with '/' shoud have \
format '/x' or '/x.x.x.x' and be valid netmask"]
                        ])
    end
  end

  describe '#net_includes' do
    it 'work correct with valid params' do
      src_addr = ip_address_group('10.212.1.1,10.212.1.101,192.168.5.15,192.168.3.0/25')
      trg_addr = ip_address_group('10.212.1.0/24,10.212.4.0/24,192.168.5.15')
      expect(net_includes(src_addr, trg_addr, false)).to eq([
                                                              [{ title: 'All included nets:',
                                                                 body:
                                                                ['10.212.1.1/32',
                                                                 '10.212.1.101/32',
                                                                 '192.168.5.15/32'] },
                                                               { title: 'All nets, which not included in another:',
                                                                 body: ['192.168.3.0/25'] }], []
                                                            ])
    end
    it 'handle detailed key' do
      src_addr = ip_address_group('10.212.1.1,10.212.1.101,192.168.5.15,192.168.3.0/25')
      trg_addr = ip_address_group('10.212.1.0/24,10.212.4.0/24,192.168.5.15')
      expect(net_includes(src_addr,
                          trg_addr,
                          true)).to eq([[{ title: 'All included nets:',
                                           body: ['10.212.1.1/32',
                                                  '10.212.1.101/32',
                                                  '192.168.5.15/32'] },
                                         { title: 'All nets, which not included in another:',
                                           body: ['192.168.3.0/25'] },
                                         { title: 'All nets, which not included in another:',
                                           body: ['10.212.1.1/32      >>included in>> 10.212.1.0/24     ',
                                                  '10.212.1.101/32    >>included in>> 10.212.1.0/24     ',
                                                  '192.168.5.15/32    >>included in>> 192.168.5.15/32   '] },
                                         { title: 'Networks, which not include any ones:',
                                           body: ['10.212.4.0/24'] }],
                                        []])
    end
  end

  describe '#net_info' do
    it 'work correct with valid params' do
      src = ip_address_group('10.212.1.1,10.155.2.0/23')
      res = net_info(src, false)
      expect(res).to eq([
                          [{ header: '10.212.1.1/32' },
                           { title: 'address:',
                             body: ['10.212.1.1'] },
                           { title: 'netmask:',
                             body: ['32', '255.255.255.255'] },
                           { title: 'network address:',
                             body: ['10.212.1.1'] },
                           { title: 'host min:',
                             body: ['10.212.1.1'] },
                           { title: 'host max:',
                             body: ['10.212.1.1'] },
                           { title: 'maximum hosts count:',
                             body: ['1'] },

                           { header: '10.155.2.0/23' },
                           { title: 'address:',
                             body: ['10.155.2.0'] },
                           { title: 'netmask:',
                             body: ['23', '255.255.254.0'] },
                           { title: 'network address:',
                             body: ['10.155.2.0'] },
                           { title: 'host min:',
                             body: ['10.155.2.1'] },
                           { title: 'host max:',
                             body: ['10.155.3.254'] },
                           { title: 'maximum hosts count:',
                             body: ['512'] }],
                          []
                        ])
    end
    it 'hande detailed key' do
      res = net_info(ip_address_group('10.10.2.5,10.24.3.0/23'), true)
      expect(res).to eq([[
                          { header: '10.10.2.5/32' },
                          { title: 'address:',
                            body: ['10.10.2.5',
                                   '00001010.00001010.00000010.00000101',
                                   'a.a.2.5',
                                   '12.12.2.5'] },
                          { title: 'netmask:',
                            body: ['32',
                                   '255.255.255.255',
                                   '11111111.11111111.11111111.11111111',
                                   'ff.ff.ff.ff',
                                   '377.377.377.377'] },
                          { title: 'wildcard:',
                            body: ['0.0.0.0',
                                   '00000000.00000000.00000000.00000000',
                                   '0.0.0.0',
                                   '0.0.0.0'] },
                          { title: 'network address:',
                            body: ['10.10.2.5',
                                   '00001010.00001010.00000010.00000101',
                                   'a.a.2.5',
                                   '12.12.2.5'] },
                          { title: 'broadcast address:',
                            body: ['10.10.2.5',
                                   '00001010.00001010.00000010.00000101',
                                   'a.a.2.5',
                                   '12.12.2.5'] },
                          { title: 'host min:',
                            body: ['10.10.2.5',
                                   '00001010.00001010.00000010.00000101',
                                   'a.a.2.5',
                                   '12.12.2.5'] },
                          { title: 'host max:',
                            body: ['10.10.2.5',
                                   '00001010.00001010.00000010.00000101',
                                   'a.a.2.5',
                                   '12.12.2.5'] },
                          { title: 'maximum hosts count:',
                            body: %w(1 1 1 1) },
                          { title: 'reverse zone address:',
                            body: ['5.2.10.10.in-addr.arpa',
                                   '00000101.00000010.00001010.00001010.in-addr.arpa',
                                   '5.2.a.a.in-addr.arpa',
                                   '5.2.12.12.in-addr.arpa'] },

                          { header: '10.24.3.0/23' },
                          { title: 'address:',
                            body: ['10.24.3.0',
                                   '00001010.00011000.00000011.00000000',
                                   'a.18.3.0',
                                   '12.30.3.0'] },
                          { title: 'netmask:',
                            body: ['23',
                                   '255.255.254.0',
                                   '11111111.11111111.11111110.00000000',
                                   'ff.ff.fe.0',
                                   '377.377.376.0'] },
                          { title: 'wildcard:',
                            body: ['0.0.1.255',
                                   '00000000.00000000.00000001.11111111',
                                   '0.0.1.ff',
                                   '0.0.1.377'] },
                          { title: 'network address:',
                            body: ['10.24.2.0',
                                   '00001010.00011000.00000010.00000000',
                                   'a.18.2.0',
                                   '12.30.2.0'] },
                          { title: 'broadcast address:',
                            body: ['10.24.3.255',
                                   '00001010.00011000.00000011.11111111',
                                   'a.18.3.ff',
                                   '12.30.3.377'] },
                          { title: 'host min:',
                            body: ['10.24.2.1',
                                   '00001010.00011000.00000010.00000001',
                                   'a.18.2.1',
                                   '12.30.2.1'] },
                          { title: 'host max:',
                            body: ['10.24.3.254',
                                   '00001010.00011000.00000011.11111110',
                                   'a.18.3.fe',
                                   '12.30.3.376'] },
                          { title: 'maximum hosts count:',
                            body: %w(512
                                     1000000000
                                     200
                                     1000) },
                          { title: 'reverse zone address:',
                            body: ['0.3.24.10.in-addr.arpa',
                                   '00000000.00000011.00011000.00001010.in-addr.arpa',
                                   '0.3.18.a.in-addr.arpa',
                                   '0.3.30.12.in-addr.arpa'] }
                        ],
                         []])
    end
    context 'handle kind' do
      before :all do
        @src = ip_address_group('10.10.2.5,10.24.3.0/23')
      end
      context 'test different call wariant:' do
        before :all do
          # 1st param of net info should be ary, or each will handle each address in network
          @src_one = [IPAddress::IPv4.new('10.212.50.2/22')]
        end
        let(:address_result) do
          [[{ header: '10.212.50.2/22', body: ['10.212.50.2'] }], []]
        end
        let(:netmask_result) do
          [[{ header: '10.212.50.2/22', body: ['255.255.252.0'] }], []]
        end
        let(:broadcast_result) do
          [[{ header: '10.212.50.2/22', body: ['10.212.51.255'] }], []]
        end
        let(:min_host_result) do
          [[{ header: '10.212.50.2/22', body: ['10.212.48.1'] }], []]
        end
        let(:max_host_result) do
          [[{ header: '10.212.50.2/22', body: ['10.212.51.254'] }], []]
        end
        let(:host_count_result) do
          [[{ header: '10.212.50.2/22', body: ['1024'] }], []]
        end
        let(:wildcard_result) do
          [[{ header: '10.212.50.2/22', body: ['0.0.3.255'] }], []]
        end
        let(:reverseaddress_result) do
          [[{ header: '10.212.50.2/22', body: ['2.50.212.10.in-addr.arpa'] }], []]
        end

        it 'address' do
          res = net_info(@src_one, false, %w(address 10))
          expect(res).to eq(address_result)
        end
        it 'a' do
          res = net_info(@src_one, false, %w(a 10))
          expect(res).to eq(address_result)
        end
        it 'addr' do
          res = net_info(@src_one, false, %w(addr 10))
          expect(res).to eq(address_result)
        end
        it 'netmask' do
          res = net_info(@src_one, false, %w(netmask 10))
          expect(res).to eq(netmask_result)
        end
        it 'nm' do
          res = net_info(@src_one, false, %w(nm 10))
          expect(res).to eq(netmask_result)
        end
        it 'mask' do
          res = net_info(@src_one, false, %w(mask 10))
          expect(res).to eq(netmask_result)
        end
        it 'n' do
          res = net_info(@src_one, false, %w(n 10))
          expect(res).to eq(netmask_result)
        end
        it 'broadcast' do
          res = net_info(@src_one, false, %w(broadcast 10))
          expect(res).to eq(broadcast_result)
        end
        it 'br' do
          res = net_info(@src_one, false, %w(br 10))
          expect(res).to eq(broadcast_result)
        end
        it 'min_host' do
          res = net_info(@src_one, false, %w(min_host 10))
          expect(res).to eq(min_host_result)
        end
        it 'min' do
          res = net_info(@src_one, false, %w(min 10))
          expect(res).to eq(min_host_result)
        end
        it 'max_host' do
          res = net_info(@src_one, false, %w(max_host 10))
          expect(res).to eq(max_host_result)
        end
        it 'max' do
          res = net_info(@src_one, false, %w(max 10))
          expect(res).to eq(max_host_result)
        end
        it 'host_count' do
          res = net_info(@src_one, false, %w(host_count 10))
          expect(res).to eq(host_count_result)
        end
        it 'hc' do
          res = net_info(@src_one, false, %w(hc 10))
          expect(res).to eq(host_count_result)
        end
        it 'wildcard' do
          res = net_info(@src_one, false, %w(wildcard 10))
          expect(res).to eq(wildcard_result)
        end
        it 'wc' do
          res = net_info(@src_one, false, %w(wc 10))
          expect(res).to eq(wildcard_result)
        end
        it 'reverseaddress' do
          res = net_info(@src_one, false, %w(reverseaddress 10))
          expect(res).to eq(reverseaddress_result)
        end
        it 'ra' do
          res = net_info(@src_one, false, %w(ra 10))
          expect(res).to eq(reverseaddress_result)
        end
      end
      context 'address' do
        it '10' do
          res = net_info(@src, false, %w(address 10))
          expect(res).to eq([[
                              { header: '10.10.2.5/32',
                                body: ['10.10.2.5'] },
                              { header: '10.24.3.0/23',
                                body: ['10.24.3.0'] }
                            ],
                             []])
        end
        it '2' do
          res = net_info(@src, false, %w(address 2))
          expect(res).to eq([[
                              { header: '10.10.2.5/32',
                                body: ['00001010.00001010.00000010.00000101'] },
                              { header: '10.24.3.0/23',
                                body: ['00001010.00011000.00000011.00000000'] }
                            ],
                             []])
        end
        it '8' do
          res = net_info(@src, false, %w(address 8))
          expect(res).to eq([[
                              { header: '10.10.2.5/32',
                                body: ['12.12.2.5'] },
                              { header: '10.24.3.0/23',
                                body: ['12.30.3.0'] }
                            ],
                             []])
        end
        it '16' do
          res = net_info(@src, false, %w(address 16))
          expect(res).to eq([[
                              { header: '10.10.2.5/32',
                                body: ['a.a.2.5'] },
                              { header: '10.24.3.0/23',
                                body: ['a.18.3.0'] }
                            ],
                             []])
        end
      end
      context 'netmask' do
        it 'short' do
          res = net_info(@src, false, %w(netmask short))
          expect(res).to eq([[
                              { header: '10.10.2.5/32',
                                body: ['32'] },
                              { header: '10.24.3.0/23',
                                body: ['23'] }
                            ], []])
        end
        it '10' do
          res = net_info(@src, false, %w(netmask 10))
          expect(res).to eq([[
                              { header: '10.10.2.5/32',
                                body: ['255.255.255.255'] },
                              { header: '10.24.3.0/23',
                                body: ['255.255.254.0'] }
                            ],
                             []])
        end
        it '2' do
          res = net_info(@src, false, %w(netmask 2))
          expect(res).to eq([[
                              { header: '10.10.2.5/32',
                                body: ['11111111.11111111.11111111.11111111'] },
                              { header: '10.24.3.0/23',
                                body: ['11111111.11111111.11111110.00000000'] }
                            ],
                             []])
        end
        it '8' do
          res = net_info(@src, false, %w(netmask 8))
          expect(res).to eq([[
                              { header: '10.10.2.5/32',
                                body: ['377.377.377.377'] },
                              { header: '10.24.3.0/23',
                                body: ['377.377.376.0'] }
                            ],
                             []])
        end
        it '16' do
          res = net_info(@src, false, %w(netmask 16))
          expect(res).to eq([[
                              { header: '10.10.2.5/32',
                                body: ['ff.ff.ff.ff'] },
                              { header: '10.24.3.0/23',
                                body: ['ff.ff.fe.0'] }
                            ],
                             []])
        end
      end
      context 'broadcast' do
        it '10' do
          res = net_info(@src, false, %w(broadcast 10))
          expect(res).to eq([[
                              { header: '10.10.2.5/32',
                                body: ['10.10.2.5'] },
                              { header: '10.24.3.0/23',
                                body: ['10.24.3.255'] }
                            ],
                             []])
        end
        it '2' do
          res = net_info(@src, false, %w(broadcast 2))
          expect(res).to eq([[
                              { header: '10.10.2.5/32',
                                body: ['00001010.00001010.00000010.00000101'] },
                              { header: '10.24.3.0/23',
                                body: ['00001010.00011000.00000011.11111111'] }
                            ],
                             []])
        end
        it '8' do
          res = net_info(@src, false, %w(broadcast 8))
          expect(res).to eq([[
                              { header: '10.10.2.5/32',
                                body: ['12.12.2.5'] },
                              { header: '10.24.3.0/23',
                                body: ['12.30.3.377'] }
                            ],
                             []])
        end
        it '16' do
          res = net_info(@src, false, %w(broadcast 16))
          expect(res).to eq([[
                              { header: '10.10.2.5/32',
                                body: ['a.a.2.5'] },
                              { header: '10.24.3.0/23',
                                body: ['a.18.3.ff'] }
                            ],
                             []])
        end
      end
      context 'min_host' do
        it '10' do
          res = net_info(@src, false, %w(min_host 10))
          expect(res).to eq([[
                              { header: '10.10.2.5/32',
                                body: ['10.10.2.5'] },
                              { header: '10.24.3.0/23',
                                body: ['10.24.2.1'] }
                            ],
                             []])
        end
        it '2' do
          res = net_info(@src, false, %w(min_host 2))
          expect(res).to eq([[{ header: '10.10.2.5/32',
                                body: ['00001010.00001010.00000010.00000101'] },
                              { header: '10.24.3.0/23',
                                body: ['00001010.00011000.00000010.00000001'] }], []])
        end
        it '8' do
          res = net_info(@src, false, %w(min_host 8))
          expect(res).to eq([[{ header: '10.10.2.5/32',
                                body: ['12.12.2.5'] },
                              { header: '10.24.3.0/23',
                                body: ['12.30.2.1'] }], []])
        end
        it '16' do
          res = net_info(@src, false, %w(min_host 16))
          expect(res).to eq([[{ header: '10.10.2.5/32',
                                body: ['a.a.2.5'] },
                              { header: '10.24.3.0/23',
                                body: ['a.18.2.1'] }], []])
        end
      end
      context 'max_host' do
        it '10' do
          res = net_info(@src, false, %w(max_host 10))
          expect(res).to eq([[
                              { header: '10.10.2.5/32',
                                body: ['10.10.2.5'] },
                              { header: '10.24.3.0/23',
                                body: ['10.24.3.254'] }
                            ],
                             []])
        end
        it '2' do
          res = net_info(@src, false, %w(max_host 2))
          expect(res).to eq([[{ header: '10.10.2.5/32',
                                body: ['00001010.00001010.00000010.00000101'] },
                              { header: '10.24.3.0/23',
                                body: ['00001010.00011000.00000011.11111110'] }], []])
        end
        it '8' do
          res = net_info(@src, false, %w(max_host 8))
          expect(res).to eq([[{ header: '10.10.2.5/32',
                                body: ['12.12.2.5'] },
                              { header: '10.24.3.0/23',
                                body: ['12.30.3.376'] }], []])
        end
        it '16' do
          res = net_info(@src, false, %w(max_host 16))
          expect(res).to eq([[{ header: '10.10.2.5/32',
                                body: ['a.a.2.5'] },
                              { header: '10.24.3.0/23',
                                body: ['a.18.3.fe'] }], []])
        end
      end
      context 'host_count' do
        it '10' do
          res = net_info(@src, false, %w(host_count 10))
          expect(res).to eq([[
                              { header: '10.10.2.5/32',
                                body: ['1'] },
                              { header: '10.24.3.0/23',
                                body: ['512'] }
                            ],
                             []])
        end
        it '2' do
          res = net_info(@src, false, %w(host_count 2))
          expect(res).to eq([[
                              { header: '10.10.2.5/32',
                                body: ['1'] },
                              { header: '10.24.3.0/23',
                                body: ['1000000000'] }
                            ],
                             []])
        end
        it '8' do
          res = net_info(@src, false, %w(host_count 8))
          expect(res).to eq([[
                              { header: '10.10.2.5/32',
                                body: ['1'] },
                              { header: '10.24.3.0/23',
                                body: ['1000'] }
                            ],
                             []])
        end
        it '16' do
          res = net_info(@src, false, %w(host_count 16))
          expect(res).to eq([[
                              { header: '10.10.2.5/32',
                                body: ['1'] },
                              { header: '10.24.3.0/23',
                                body: ['200'] }
                            ],
                             []])
        end
      end
      context 'wildcard' do
        it '10' do
          res = net_info(@src, false, %w(wildcard 10))
          expect(res).to eq([[
                              { header: '10.10.2.5/32',
                                body: ['0.0.0.0'] },
                              { header: '10.24.3.0/23',
                                body: ['0.0.1.255'] }
                            ],
                             []])
        end
        it '2' do
          res = net_info(@src, false, %w(wildcard 2))
          expect(res).to eq([[
                              { header: '10.10.2.5/32',
                                body: ['00000000.00000000.00000000.00000000'] },
                              { header: '10.24.3.0/23',
                                body: ['00000000.00000000.00000001.11111111'] }
                            ],
                             []])
        end
        it '8' do
          res = net_info(@src, false, %w(wildcard 8))
          expect(res).to eq([[
                              { header: '10.10.2.5/32',
                                body: ['0.0.0.0'] },
                              { header: '10.24.3.0/23',
                                body: ['0.0.1.377'] }
                            ], []])
        end
        it '16' do
          res = net_info(@src, false, %w(wildcard 16))
          expect(res).to eq([[
                              { header: '10.10.2.5/32',
                                body: ['0.0.0.0'] },
                              { header: '10.24.3.0/23',
                                body: ['0.0.1.ff'] }
                            ],
                             []])
        end
      end
      context 'reverse address' do
        it '10' do
          res = net_info(@src, false, %w(reverseaddress 10))
          expect(res).to eq([[
                              { header: '10.10.2.5/32',
                                body: ['5.2.10.10.in-addr.arpa'] },
                              { header: '10.24.3.0/23',
                                body: ['0.3.24.10.in-addr.arpa'] }
                            ],
                             []])
        end
        it '2' do
          res = net_info(@src, false, %w(reverseaddress 2))
          expect(res).to eq([[
                              { header: '10.10.2.5/32',
                                body: ['00000101.00000010.00001010.00001010.in-addr.arpa'] },
                              { header: '10.24.3.0/23',
                                body: ['00000000.00000011.00011000.00001010.in-addr.arpa'] }
                            ],
                             []])
        end
        it '8' do
          res = net_info(@src, false, %w(reverseaddress 8))
          expect(res).to eq([[
                              { header: '10.10.2.5/32',
                                body: ['5.2.12.12.in-addr.arpa'] },
                              { header: '10.24.3.0/23',
                                body: ['0.3.30.12.in-addr.arpa'] }
                            ],
                             []])
        end
        it '16' do
          res = net_info(@src, false, %w(reverseaddress 16))
          expect(res).to eq([[
                              { header: '10.10.2.5/32',
                                body: ['5.2.a.a.in-addr.arpa'] },
                              { header: '10.24.3.0/23',
                                body: ['0.3.18.a.in-addr.arpa'] }
                            ],
                             []])
        end
      end
    end
  end
  describe '#supernet' do
    before :all do
      @src = ip_address_group('10.10.2.3/24,10.10.4.0/23,10.10.10.0/22')
    end
    it 'work correct with valid params' do
      res = supernet(@src, 21, false)
      expect(res).to eq([[{ header: 'All supernets:' },
                          { body: '10.10.0.0/21' },
                          { body: '10.10.0.0/21' },
                          { body: '10.10.8.0/21' }],
                         []])
    end
    it 'generate valid errors count' do
      res = supernet(@src, 23, false)
      expect(res).to eq([[{ header: 'All supernets:' },
                          { body: '10.10.2.0/23' }],
                         ['There were 2 nets, which not handled because of target subnet mask greater then source']])
    end
    it 'handle --detailed key' do
      res = supernet(@src, 21, true)
      expect(res).to eq([[{ header: 'All supernets:' },
                          { body: '10.10.2.3/24       => 10.10.0.0/21      ' },
                          { body: '10.10.4.0/23       => 10.10.0.0/21      ' },
                          { body: '10.10.10.0/22      => 10.10.8.0/21      ' }], []])
    end
  end
  describe '#summarization' do
    it 'correct work with valid params' do
      res = summarization(ip_address_group('10.212.1.1/24,10.212.2.1/24,10.212.3.3/24'))
      expect(res).to eq([[{ body: ['10.212.1.0/24', '10.212.2.0/23'] }], []])
    end
  end
  describe '#subnets' do
    before :all do
      @src = ip_address_group('10.212.1.0/23,10.212.5.0/24,10.212.6.0/24')
    end
    it 'work correct with valid params' do
      res = subnets(@src, 25)
      expect(res).to eq([[
                          { title: '10.212.1.0/23',
                            body: ['10.212.0.0/25',
                                   '10.212.0.128/25',
                                   '10.212.1.0/25',
                                   '10.212.1.128/25'] },
                          { title: '10.212.5.0/24',
                            body: ['10.212.5.0/25', '10.212.5.128/25'] },
                          { title: '10.212.6.0/24',
                            body: ['10.212.6.0/25', '10.212.6.128/25'] }
                        ],
                         []])
    end
    it 'generate correct error count' do
      res = subnets(@src, 23)
      expect(res).to eq([[
                          { title: '10.212.1.0/23',
                            body: ['10.212.0.0/23'] }
                        ],
                         ['There were 2 nets, which not handled because target subnet mask smaller then source']])
    end
  end
  describe '#split' do
    it 'work correct with valid params' do
      res = split(ip_address_group('10.212.0.0/25'), 9)
      expect(res).to eq([[
                          { title: '10.212.0.0/25',
                            body: ['10.212.0.0/29',
                                   '10.212.0.8/29',
                                   '10.212.0.16/29',
                                   '10.212.0.24/29',
                                   '10.212.0.32/29',
                                   '10.212.0.40/29',
                                   '10.212.0.48/29',
                                   '10.212.0.56/29',
                                   '10.212.0.64/26'] }
                        ],
                         []])
    end
    it 'produce errors whe send incorrect params' do
      res = split(ip_address_group('192.168.1.3/32,10.212.0.0/25,10.53.4.5/30'), 9)
      expect(res).to eq(
        [[
          { title: '10.212.0.0/25',
            body: ['10.212.0.0/29',
                   '10.212.0.8/29',
                   '10.212.0.16/29',
                   '10.212.0.24/29',
                   '10.212.0.32/29',
                   '10.212.0.40/29',
                   '10.212.0.48/29',
                   '10.212.0.56/29',
                   '10.212.0.64/26'] }
        ],
         ['It not possible to split net 192.168.1.3/32 on 9 networks',
          'It not possible to split net 10.53.4.5/30 on 9 networks']]
      )
    end
  end
end
