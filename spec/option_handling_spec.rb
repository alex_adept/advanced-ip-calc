describe 'option handling' do
  describe '#get_help_name' do
    it 'working test' do
      res = get_help_name('test')
      expect(res).to eq('OPTION_TEST_HELP')
    end
  end
  describe '.OptionNamer' do
    describe '#get_variants' do
      it 'simple call' do
        res = OptionNamer.get_variants('nocolor', :kind)
        expect(res).to eq([:nocolor, :k, :kind])
      end
    end
    describe '#get_name' do
      it 'works with symbol' do
        res = OptionNamer.get_name(:src)
        expect(res).to eq(:'src-group')
      end
      it 'works with string' do
        res = OptionNamer.get_name('src')
        expect(res).to eq(:'src-group')
      end
      it 'handle :src' do
        res = OptionNamer.get_name('src')
        expect(res).to eq(:'src-group')
      end
      it 'handle s:' do
        res = OptionNamer.get_name('s')
        expect(res).to eq(:'src-group')
      end
      it 'handle :src-group' do
        res = OptionNamer.get_name('src-group')
        expect(res).to eq(:'src-group')
      end
      it 'handle :t' do
        res = OptionNamer.get_name('t')
        expect(res).to eq(:'target-group')
      end
      it 'handle :target-group' do
        res = OptionNamer.get_name('target-group')
        expect(res).to eq(:'target-group')
      end
      it 'handle :o' do
        res = OptionNamer.get_name('o')
        expect(res).to eq(:operation)
      end
      it 'handle :operation' do
        res = OptionNamer.get_name('operation')
        expect(res).to eq(:operation)
      end
      it 'handle :number' do
        res = OptionNamer.get_name('number')
        expect(res).to eq(:number)
      end
      it 'handle :n' do
        res = OptionNamer.get_name('n')
        expect(res).to eq(:number)
      end
      it 'handle :netmask' do
        res = OptionNamer.get_name('netmask')
        expect(res).to eq(:number)
      end
      it 'handle :detailed' do
        res = OptionNamer.get_name('detailed')
        expect(res).to eq(:detailed)
      end
      it 'handle :d' do
        res = OptionNamer.get_name('d')
        expect(res).to eq(:detailed)
      end
      it 'handle :k' do
        res = OptionNamer.get_name('k')
        expect(res).to eq(:kind)
      end
      it 'handle :kind' do
        res = OptionNamer.get_name('kind')
        expect(res).to eq(:kind)
      end
      it 'handle :hide-title' do
        res = OptionNamer.get_name('hide-title')
        expect(res).to eq(:'hide-title')
      end
      it 'handle :h' do
        res = OptionNamer.get_name('h')
        expect(res).to eq(:'hide-title')
      end
      it 'handle :nocolor' do
        res = OptionNamer.get_name('nocolor')
        expect(res).to eq(:nocolor)
      end
      it 'handle :nouniq' do
        res = OptionNamer.get_name('nouniq')
        expect(res).to eq(:nouniq)
      end
      it 'handle :version' do
        res = OptionNamer.get_name('version')
        expect(res).to eq(:version)
      end
      it 'handle :includes' do
        res = OptionNamer.get_name('includes')
        expect(res).to eq(:includes)
      end
      it 'handle :info' do
        res = OptionNamer.get_name('info')
        expect(res).to eq(:info)
      end
      it 'handle :split' do
        res = OptionNamer.get_name('split')
        expect(res).to eq(:split)
      end
      it 'handle :subnet' do
        res = OptionNamer.get_name('subnet')
        expect(res).to eq(:subnets)
      end
      it 'handle :sub' do
        res = OptionNamer.get_name('sub')
        expect(res).to eq(:subnets)
      end
      it 'handle :subnets' do
        res = OptionNamer.get_name('subnets')
        expect(res).to eq(:subnets)
      end
      it 'handle :supernet' do
        res = OptionNamer.get_name('supernet')
        expect(res).to eq(:supernet)
      end
      it 'handle :super' do
        res = OptionNamer.get_name('super')
        expect(res).to eq(:supernet)
      end
      it 'handle :sup' do
        res = OptionNamer.get_name('sup')
        expect(res).to eq(:supernet)
      end
      it 'handle :summarization' do
        res = OptionNamer.get_name('summarization')
        expect(res).to eq(:summarization)
      end
      it 'handle :summ' do
        res = OptionNamer.get_name('summ')
        expect(res).to eq(:summarization)
      end
      it 'handle :sum' do
        res = OptionNamer.get_name('sum')
        expect(res).to eq(:summarization)
      end
    end
  end
end
