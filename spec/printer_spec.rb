require_relative '../lib/printer'

describe '.printer class' do
  before :each do
    @printer = Printer.new
    @printer.new_group
  end
  context 'formatter methods:' do
    before :each do
      @str = 'qwe'
    end
    it '#colorize' do
      res = @printer.send(:colorize, RED, @str)
      expect(res).to eq("\e[31mqwe\e[0m")
    end
    it '#title' do
      res = @printer.send(:title, @str)
      expect(res).to eq("\e[32mQWE\e[0m")
    end
    it '#title!' do
      @printer.send(:title!, @str)
      expect(@str).to eq("\e[32mQWE\e[0m")
    end
    it '#supertitle' do
      res = @printer.send(:supertitle, @str)
      expect(res).to eq("\n\e[44mQWE\e[0m")
    end
    it '#supertitle!' do
      @printer.send(:supertitle!, @str)
      expect(@str).to eq("\n\e[44mQWE\e[0m")
    end
    it '#footter' do
      res = @printer.send(:footter, @str)
      expect(res).to eq("\e[35mqwe\e[0m")
    end
    it '#footter!' do
      @printer.send(:footter!, @str)
      expect(@str).to eq("\e[35mqwe\e[0m")
    end
  end

  context 'data methods:' do
    it '#new_group' do
      expect { @printer.new_group }.to change { @printer.main_data.count }.by(1)
    end
    it '#push_header' do
      expect { @printer.push_header('testH') }.to change { @printer.main_data.last }
        .from(body: [])
        .to(body: [], header: 'testH')
    end
    it '#common_footter= with string should convert data to array' do
      expect { @printer.common_footter = 'CFootter' }.to change { @printer.common_footter }.from([]).to(['CFootter'])
    end
    it '#common_footter= with array' do
      expect { @printer.common_footter = %w(CFootter1 CFootter2) }.to change { @printer.common_footter }
        .from([])
        .to(%w(CFootter1 CFootter2))
    end
    it '#push_title' do
      expect { @printer.push_title('TiTle') }.to change { @printer.main_data.last }
        .from(body: [])
        .to(body: [], title: 'TiTle')
    end
    it '#push_body' do
      @printer.push_body('body1')
      @printer.push_body('body2')
      expect(@printer.main_data.last).to eq(body: %w(body1 body2))
    end
    it '#push_footter' do
      expect { @printer.push_footter('FooTTeR') }.to change { @printer.main_data.last }
        .from(body: [])
        .to(body: [], footter: 'FooTTeR')
    end
    it '#push_group' do
      expected = expect do
        @printer.push_group(body: %w(b1 b2),
                            title: 't',
                            footter: %w(f1 f2),
                            header: 'h1')
      end
      expected.to change { @printer.main_data }.from([{ body: [] }]).to([{ body: [] }, { body: %w(b1 b2),
                                                                                         title: 't',
                                                                                         footter: %w(f1 f2),
                                                                                         header: 'h1' }])
    end
    it '#push_groups' do
      g1 = { body: %w(b11 b12), title: 't1' }
      g2 = { header: 'h1', body: %w(b21 b22), footter: %w(f21 f22) }
      expect { @printer.push_groups([g1, g2]) }.to change { @printer.main_data }.from([{ body: [] }])
        .to([{ body: [] },
             { body: %w(b11 b12), title: 't1' },
             { header: 'h1', body: %w(b21 b22), footter: %w(f21 f22) }])
    end
    it '#correct_user_input' do
      g1 = { body: 'b11', title: 't1' }
      g2 = { header: 'h1', body: %w(b21 b22), footter: 'f21' }
      @printer.push_groups([g1, g2])
      res = [{ body: [] },
             { body: ['b11'], title: 't1' },
             { header: 'h1', body: %w(b21 b22), footter: ['f21'] }]
      expect(@printer.send(:correct_user_input)).to eq(res)
    end
  end
  context '#print_all' do
    before :each do
      @g1 = { body: 'b11', title: 't1' }
      @g2 = { header: 'h1', body: %w(b21 b22), footter: 'f21' }
    end
    it 'work correct with default values' do
      @printer.push_groups [@g1, @g1]
      output = "\n\e[44m\e[0m\n\n\e[32m\e[32MT1\e[0M\e[0m\n\tb11\n\e[32m\e[32MT1\e[0M\e[0m\n\tb11\n"
      expect { @printer.print_all }.to output(output).to_stdout
    end
    it '+nocolorize' do
      @printer.push_groups [@g1, @g1]
      expect { @printer.print_all(true) }.to output("\n\nt1\n\tb11\nt1\n\tb11\n").to_stdout
    end
    it 'hide_title' do
      @printer.push_groups [@g1, @g1]
      expect { @printer.print_all(false, true) }.to output("\n\nb11\n\nb11\n\n").to_stdout
    end
  end
end
