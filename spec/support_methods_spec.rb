require 'ipaddress'
require_relative '../lib/support_methods'
require_relative '../lib/help_messages'
require_relative '../lib/option_handling'

# IPAddress::IPv4.new()

describe 'test support_methods' do
  before :all do
    @addr = IPAddress::IPv4.new('10.212.1.1/24')
  end
  describe '#get_addr' do
    it 'notation 10' do
      expect(get_addr(@addr, 10)).to eq(['10.212.1.1'])
    end

    it 'notation 2' do
      expect(get_addr(@addr, 2)).to eq(['00001010.11010100.00000001.00000001'])
    end

    it 'notation 8' do
      expect(get_addr(@addr, 8)).to eq(['12.324.1.1'])
    end

    it 'notation 16' do
      expect(get_addr(@addr, 16)).to eq(['a.d4.1.1'])
    end
  end
  describe '#get_net_addr' do
    it 'notation 10' do
      expect(get_net_addr(@addr, 10)).to eq(['10.212.1.0'])
    end

    it 'notation 2' do
      expect(get_net_addr(@addr, 2)).to eq(['00001010.11010100.00000001.00000000'])
    end

    it 'notation 8' do
      expect(get_net_addr(@addr, 8)).to eq(['12.324.1.0'])
    end

    it 'notation 16' do
      expect(get_net_addr(@addr, 16)).to eq(['a.d4.1.0'])
    end
  end
  describe '#get_broadcast' do
    it 'notation 10' do
      expect(get_broadcast(@addr, 10)).to eq(['10.212.1.255'])
    end

    it 'notation 2' do
      expect(get_broadcast(@addr, 2)).to eq(['00001010.11010100.00000001.11111111'])
    end

    it 'notation 8' do
      expect(get_broadcast(@addr, 8)).to eq(['12.324.1.377'])
    end

    it 'notation 16' do
      expect(get_broadcast(@addr, 16)).to eq(['a.d4.1.ff'])
    end
  end
  describe '#get_min_host' do
    it 'notation 10' do
      expect(get_min_host(@addr, 10)).to eq(['10.212.1.1'])
    end

    it 'notation 2' do
      expect(get_min_host(@addr, 2)).to eq(['00001010.11010100.00000001.00000001'])
    end

    it 'notation 8' do
      expect(get_min_host(@addr, 8)).to eq(['12.324.1.1'])
    end

    it 'notation 16' do
      expect(get_min_host(@addr, 16)).to eq(['a.d4.1.1'])
    end
  end
  describe '#get_max_host' do
    it 'notation 10' do
      expect(get_max_host(@addr, 10)).to eq(['10.212.1.254'])
    end

    it 'notation 2' do
      expect(get_max_host(@addr, 2)).to eq(['00001010.11010100.00000001.11111110'])
    end

    it 'notation 8' do
      expect(get_max_host(@addr, 8)).to eq(['12.324.1.376'])
    end

    it 'notation 16' do
      expect(get_max_host(@addr, 16)).to eq(['a.d4.1.fe'])
    end
  end
  describe '#get_host_count' do
    it 'notation 10' do
      expect(get_host_count(@addr, 10)).to eq(['256'])
    end

    it 'notation 2' do
      expect(get_host_count(@addr, 2)).to eq(['100000000'])
    end

    it 'notation 8' do
      expect(get_host_count(@addr, 8)).to eq(['400'])
    end

    it 'notation 16' do
      expect(get_host_count(@addr, 16)).to eq(['100'])
    end
  end
  describe '#get_wildcard' do
    it 'notation 10' do
      expect(get_wildcard(@addr, 10)).to eq(['0.0.0.255'])
    end

    it 'notation 2' do
      expect(get_wildcard(@addr, 2)).to eq(['00000000.00000000.00000000.11111111'])
    end

    it 'notation 8' do
      expect(get_wildcard(@addr, 8)).to eq(['0.0.0.377'])
    end

    it 'notation 16' do
      expect(get_wildcard(@addr, 16)).to eq(['0.0.0.ff'])
    end
  end
  describe '#get_reverse_addr' do
    it 'notation 10' do
      expect(get_reverse_addr(@addr, 10)).to eq(['1.1.212.10.in-addr.arpa'])
    end

    it 'notation 2' do
      expect(get_reverse_addr(@addr, 2)).to eq(['00000001.00000001.11010100.00001010.in-addr.arpa'])
    end

    it 'notation 8' do
      expect(get_reverse_addr(@addr, 8)).to eq(['1.1.324.12.in-addr.arpa'])
    end

    it 'notation 16' do
      expect(get_reverse_addr(@addr, 16)).to eq(['1.1.d4.a.in-addr.arpa'])
    end
  end
  describe '#octets_to_notation' do
    it 'to_10' do
      expect(octets_to_notation(@addr.octets, 10)).to eq('10.212.1.1')
    end

    it 'to_2' do
      expect(octets_to_notation(@addr.octets, 2)).to eq('00001010.11010100.00000001.00000001')
    end

    it 'to_8' do
      expect(octets_to_notation(@addr.octets, 8)).to eq('12.324.1.1')
    end

    it 'to_16' do
      expect(octets_to_notation(@addr.octets, 16)).to eq('a.d4.1.1')
    end
  end

  describe '#check_params produce error when' do
    it 'src is nil' do
      options = { operation: :info, src: nil  }
      res = ["Parametre '-s' should have correct format and can not be empty. '--help s' for details"]
      expect(OptionChecker.new(options).valid?).to eq(res)
    end

    it 'src is empty' do
      options = { operation: :info, src: [] }
      res = ["Parametre '-s' should have correct format and can not be empty. '--help s' for details"]
      expect(OptionChecker.new(options).valid?).to eq(res)
    end

    it 'trg exist but nil' do
      options = { src: [IPAddress::IPv4.new('10.212.1.1/24')],
                  trg: nil,
                  operation: :info }
      res = ["Parametre '-t' should have correct format and can not be empty. '--help t' for details"]
      expect(OptionChecker.new(options).valid?).to eq(res)
    end

    it 'trg exist but empty' do
      options = { src: [IPAddress::IPv4.new('10.212.1.1/24')],
                  trg: [],
                  operation: :info }
      res = ["Parametre '-t' should have correct format and can not be empty. '--help t' for details"]
      expect(OptionChecker.new(options).valid?).to eq(res)
    end

    it 'incorrect ktype' do
      options = { src: [IPAddress::IPv4.new('10.212.1.1/24')],
                  kind: %w(netnet 2),
                  operation: :info }
      res = ["Type in --kind (-k) option incorrect. See '--help k' for more info"]
      expect(OptionChecker.new(options).valid?).to eq(res)
    end

    it 'incorrect knotate' do
      options = { src: [IPAddress::IPv4.new('10.212.1.1/24')],
                  kind: %w(addr 12),
                  operation: :info }
      res = ["Notation in --kind (-k) option incorrect. See '--help k' for more info"]
      expect(OptionChecker.new(options).valid?).to eq(res)
    end

    it 'kind can be used only with info' do
      options = { src: [IPAddress::IPv4.new('10.212.1.1/24')],
                  trg: [IPAddress::IPv4.new('10.212.1.0/22')],
                  kind: %w(addr 2),
                  operation: :includes }
      res = ["Option --kind (-k) can be used only with 'info' operation"]
      expect(OptionChecker.new(options).valid?).to eq(res)
    end

    context 'number not present for operation' do
      let(:errors) do
        "operation you should pass '--number (-n)' param"
      end

      def g_options(mname)
        { src: [IPAddress::IPv4.new('10.212.1.1/24')], trg: [IPAddress::IPv4.new('10.212.1.0/22')], operation: mname }
      end

      it 'split' do
        options = g_options(:split)
        expect(OptionChecker.new(options).valid?[0]).to include(errors)
      end

      it 'super' do
        options = g_options(:super)
        expect(OptionChecker.new(options).valid?[0]).to include(errors)
      end

      it 'sup' do
        options = g_options(:sup)
        expect(OptionChecker.new(options).valid?[0]).to include(errors)
      end

      it 'supernet' do
        options = g_options(:supernet)
        expect(OptionChecker.new(options).valid?[0]).to include(errors)
      end

      it 'sub' do
        options = g_options(:sub)
        expect(OptionChecker.new(options).valid?[0]).to include(errors)
      end

      it 'subnet' do
        options = g_options(:subnet)
        expect(OptionChecker.new(options).valid?[0]).to include(errors)
      end
    end
    context 'number is not integer for' do
      def errors(o_name)
        ['--number (-n) param shold be integer in this usage',
         "--number (-n) param for #{o_name} is netmask and should be between 1 and 32"]
      end

      def g_options(mname)
        { src: [IPAddress::IPv4.new('10.212.1.1/24')],
          trg: [IPAddress::IPv4.new('10.212.1.0/22')],
          operation: mname, number: 'qwe' }
      end

      it 'split' do
        options = g_options(:split)
        error = ['--number (-n) param shold be integer in this usage']
        expect(OptionChecker.new(options).valid?).to eq(error)
      end

      it 'super' do
        options = g_options(:super)
        expect(OptionChecker.new(options).valid?).to eq(errors(:super))
      end

      it 'sup' do
        options = g_options(:sup)
        expect(OptionChecker.new(options).valid?).to eq(errors(:sup))
      end

      it 'supernet' do
        options = g_options(:supernet)
        expect(OptionChecker.new(options).valid?).to eq(errors(:supernet))
      end

      it 'sub' do
        options = g_options(:sub)
        expect(OptionChecker.new(options).valid?).to eq(errors(:sub))
      end

      it 'subnet' do
        options = g_options(:subnet)
        expect(OptionChecker.new(options).valid?).to eq(errors(:subnet))
      end
    end
    context 'number is not netmask for operation' do
      let(:errors) do
        'is netmask and should be between 1 and 32'
      end

      def g_options(mname)
        { src: [IPAddress::IPv4.new('10.212.1.1/24')],
          trg: [IPAddress::IPv4.new('10.212.1.0/22')],
          operation: mname, number: 40 }
      end

      it 'super' do
        options = g_options(:super)
        expect(OptionChecker.new(options).valid?[0]).to include(errors)
      end

      it 'sup' do
        options = g_options(:sup)
        expect(OptionChecker.new(options).valid?[0]).to include(errors)
      end

      it 'supernet' do
        options = g_options(:supernet)
        expect(OptionChecker.new(options).valid?[0]).to include(errors)
      end

      it 'sub' do
        options = g_options(:sub)
        expect(OptionChecker.new(options).valid?[0]).to include(errors)
      end

      it 'subnet' do
        options = g_options(:subnet)
        expect(OptionChecker.new(options).valid?[0]).to include(errors)
      end
    end
    context 'trg not present for' do
      def g_options(mname)
        { src: [IPAddress::IPv4.new('10.212.1.1/24')], operation: mname }
      end

      let(:error) do
        "method you should pass '--target-group (-t) parametre'"
      end

      it 'includes' do
        options = g_options(:includes)
        expect(OptionChecker.new(options).valid?[0]).to include(error)
      end
    end
  end

  context '#print_help' do
    it 's' do
      help_option = ['s']
      expect { print_help(help_option, nil) }.to output(OPTION_SRC_GROUP_HELP + "\n").to_stdout
    end

    it 'src-group' do
      help_option = ['src-group']
      expect { print_help(help_option, nil) }.to output(OPTION_SRC_GROUP_HELP + "\n").to_stdout
    end

    it 'src' do
      help_option = ['src']
      expect { print_help(help_option, nil) }.to output(OPTION_SRC_GROUP_HELP + "\n").to_stdout
    end

    it 't' do
      help_option = ['t']
      expect { print_help(help_option, nil) }.to output(OPTION_TARGET_GROUP_HELP + "\n").to_stdout
    end

    it 'target-group' do
      help_option = ['target-group']
      expect { print_help(help_option, nil) }.to output(OPTION_TARGET_GROUP_HELP + "\n").to_stdout
    end

    it 'n' do
      help_option = ['n']
      expect { print_help(help_option, nil) }.to output(OPTION_NUMBER_HELP + "\n").to_stdout
    end

    it 'number' do
      help_option = ['number']
      expect { print_help(help_option, nil) }.to output(OPTION_NUMBER_HELP + "\n").to_stdout
    end

    it 'number' do
      help_option = ['number']
      expect { print_help(help_option, nil) }.to output(OPTION_NUMBER_HELP + "\n").to_stdout
    end

    it 'd' do
      help_option = ['d']
      expect { print_help(help_option, nil) }.to output(OPTION_DETAILED_HELP + "\n").to_stdout
    end

    it 'detailed' do
      help_option = ['detailed']
      expect { print_help(help_option, nil) }.to output(OPTION_DETAILED_HELP + "\n").to_stdout
    end

    it 'k' do
      help_option = ['k']
      expect { print_help(help_option, nil) }.to output(OPTION_KIND_HELP + "\n").to_stdout
    end

    it 'kind' do
      help_option = ['kind']
      expect { print_help(help_option, nil) }.to output(OPTION_KIND_HELP + "\n").to_stdout
    end

    it 'h' do
      help_option = ['h']
      expect { print_help(help_option, nil) }.to output(OPTION_HIDE_TITLE_HELP + "\n").to_stdout
    end

    it 'hide-title' do
      help_option = ['hide-title']
      expect { print_help(help_option, nil) }.to output(OPTION_HIDE_TITLE_HELP + "\n").to_stdout
    end

    it 'nocolor' do
      help_option = ['nocolor']
      expect { print_help(help_option, nil) }.to output(OPTION_NOCOLOR_HELP + "\n").to_stdout
    end

    it 'nouniq' do
      help_option = ['nouniq']
      expect { print_help(help_option, nil) }.to output(OPTION_NOUNIQ_HELP + "\n").to_stdout
    end

    it 'version' do
      help_option = ['version']
      expect { print_help(help_option, nil) }.to output(OPTION_VERSION_HELP + "\n").to_stdout
    end

    it 'info' do
      help_option = ['info']
      expect { print_help(help_option, nil) }.to output(OPTION_INFO_HELP + "\n").to_stdout
    end

    it 'includes' do
      help_option = ['includes']
      expect { print_help(help_option, nil) }.to output(OPTION_INCLUDES_HELP + "\n").to_stdout
    end

    it 'super' do
      help_option = ['super']
      expect { print_help(help_option, nil) }.to output(OPTION_SUPERNET_HELP + "\n").to_stdout
    end

    it 'sup' do
      help_option = ['sup']
      expect { print_help(help_option, nil) }.to output(OPTION_SUPERNET_HELP + "\n").to_stdout
    end

    it 'supernet' do
      help_option = ['supernet']
      expect { print_help(help_option, nil) }.to output(OPTION_SUPERNET_HELP + "\n").to_stdout
    end

    it 'sum' do
      help_option = ['sum']
      expect { print_help(help_option, nil) }.to output(OPTION_SUMMARIZATION_HELP + "\n").to_stdout
    end

    it 'summ' do
      help_option = ['summ']
      expect { print_help(help_option, nil) }.to output(OPTION_SUMMARIZATION_HELP + "\n").to_stdout
    end

    it 'summarization' do
      help_option = ['summarization']
      expect { print_help(help_option, nil) }.to output(OPTION_SUMMARIZATION_HELP + "\n").to_stdout
    end

    it 'sub' do
      help_option = ['sub']
      expect { print_help(help_option, nil) }.to output(OPTION_SUBNETS_HELP + "\n").to_stdout
    end

    it 'subnet' do
      help_option = ['subnet']
      expect { print_help(help_option, nil) }.to output(OPTION_SUBNETS_HELP + "\n").to_stdout
    end

    it 'split' do
      help_option = ['split']
      expect { print_help(help_option, nil) }.to output(OPTION_SPLIT_HELP + "\n").to_stdout
    end

    context 'operation as:' do
      it 'o' do
        help_option = ['o']
        expect { print_help(help_option, nil) }.to output(OPTION_OPERATION_HELP + "\n").to_stdout
      end

      it 'operation' do
        help_option = ['operation']
        expect { print_help(help_option, nil) }.to output(OPTION_OPERATION_HELP + "\n").to_stdout
      end

      it 'o,info' do
        help_option = %w(o info)
        expect { print_help(help_option, nil) }.to output(OPTION_INFO_HELP + "\n").to_stdout
      end

      it 'o,includes' do
        help_option = %w(o includes)
        expect { print_help(help_option, nil) }.to output(OPTION_INCLUDES_HELP + "\n").to_stdout
      end

      it 'o,super' do
        help_option = %w(o super)
        expect { print_help(help_option, nil) }.to output(OPTION_SUPERNET_HELP + "\n").to_stdout
      end

      it 'o,sup' do
        help_option = %w(o sup)
        expect { print_help(help_option, nil) }.to output(OPTION_SUPERNET_HELP + "\n").to_stdout
      end

      it 'o,supernet' do
        help_option = %w(o supernet)
        expect { print_help(help_option, nil) }.to output(OPTION_SUPERNET_HELP + "\n").to_stdout
      end

      it 'o,summ' do
        help_option = %w(o summ)
        expect { print_help(help_option, nil) }.to output(OPTION_SUMMARIZATION_HELP + "\n").to_stdout
      end

      it 'o,summarization' do
        help_option = %w(o summarization)
        expect { print_help(help_option, nil) }.to output(OPTION_SUMMARIZATION_HELP + "\n").to_stdout
      end

      it 'o,sub' do
        help_option = %w(o sub)
        expect { print_help(help_option, nil) }.to output(OPTION_SUBNETS_HELP + "\n").to_stdout
      end

      it 'o,subnet' do
        help_option = %w(o subnet)
        expect { print_help(help_option, nil) }.to output(OPTION_SUBNETS_HELP + "\n").to_stdout
      end

      it 'o,split' do
        help_option = %w(o split)
        expect { print_help(help_option, nil) }.to output(OPTION_SPLIT_HELP + "\n").to_stdout
      end
    end
  end
end
